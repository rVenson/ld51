extends Node

const audio_list = {
	"gates01": preload("res://resources/sound/fx/14_gates01.wav"),
	"step01": preload("res://resources/sound/fx/3_step01.wav"),
	"laught01": preload("res://resources/sound/fx/7_laugh01.wav"),
	"laught02": preload("res://resources/sound/fx/8_laugh02.wav"),
	"laught03": preload("res://resources/sound/fx/9_laugh03.wav"),
	"end02": preload("res://resources/sound/fx/11_end02.wav"),
	"wind01": preload("res://resources/sound/fx/15_wind01.wav"),
	"whistle": preload("res://resources/sound/fx/whistle.wav")
}

func play_audio(audio_name, random_pitch = false):
	var audio_player : AudioStreamPlayer = AudioStreamPlayer.new()
	var wav = audio_list.get(audio_name)
	audio_player.stream = wav
	if (random_pitch):
		audio_player.pitch_scale = rand_range(0.9, 1.1)
	else:
		audio_player.pitch_scale = 1.0
	add_child(audio_player)
	audio_player.play()
	yield(audio_player, 'finished')
	audio_player.queue_free()
