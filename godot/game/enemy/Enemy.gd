extends Spatial

export var initial_animation = "Idle"
export var speed = 3
onready var animation_player = $Mesh/AnimationPlayer
onready var area = $Area
onready var navigation_agent = $NavigationAgent
onready var spawn_sound = $SpawnSound
var target = null

func show():
	spawn_sound.play()
	animation_player.play(initial_animation)
	visible = true
	yield(get_tree().create_timer(2), "timeout")
	area.monitorable = true
	area.monitoring = true

func _physics_process(delta):
	if (!target) : return
	var current_position = self.global_transform.origin
	var target_position = target.global_transform.origin
	navigation_agent.set_target_location(target_position)
	var next_position = navigation_agent.get_next_location()
	var direction = current_position.direction_to(next_position)
	var distance = current_position.distance_squared_to(next_position)
	self.global_transform.origin += direction * delta * speed
	look_at(next_position, Vector3.UP)

func set_target(target):
	self.target = target

func _on_Area_body_entered(body : Node):
	if (body.is_in_group('player')):
		body.damage()
		body.set_camera_target(self)
		target = null
