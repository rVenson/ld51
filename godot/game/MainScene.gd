extends Spatial

signal game_end
onready var player = $Player
onready var animation_player = $AnimationPlayer
onready var npc = $NPC
onready var npcs_to_spawn : Array = npc.get_children()
onready var npc_timer = $NPCTimer
var current_warning_pool = []
var left_ticks = 10
var services = null

const phrases = [
	"Now that’s what I’m talking about.",
	"You look so nice.",
	"Are you single?",
	"Can I get your number?",
	"You look so good.",
	"Tonight you will be mine...",
	"You wanna some beer?",
	"I'm sure you need some alcohol.",
	"Hey, how much?",
	"If you drink something everything will be fine.",
	"Hey, you dropped something",
]
const hate_phrases = [
	"You fucking bitch!",
	"You’re a fucking ugly whore",
	"I dreamt about you. You died.",
	"For a fatty, you don’t sweat much.",
	"You’re so annoying!",
	"Come with me if you want to live!",
	"You look a lot like my next victim,"
]
const final_phrases = [
	"I knew you wouldn't resist!",
	"You don't have a choice now!",
	"Come closer... You never will escape!",
]
const loss_text = [
	"You were never seen again...",
	"You will have nightmares forever about this day",
	"You were silenced forever",
]
const win_text = [
	"You escaped safely. This time.",
	"You managed to escape. This time.",
]

func _ready():
	set_process(false)

func set_services(services):
	self.services = services

func _process(delta):
	left_ticks -= delta
	if (left_ticks < 0):
		services.audio.play_audio("laught03")
		var random_dialog = get_warning_phrases()
		services.dialogue.play_dialogue(random_dialog)
		left_ticks = 10

func create_warning_phrases_pool():
	var list = phrases.duplicate()
	var is_strong_language = Global.settings.get('strong_language', false)
	if (is_strong_language):
		list.append_array(hate_phrases)
	randomize()
	list.shuffle()
	current_warning_pool = list

func get_warning_phrases():
	if (current_warning_pool.empty()):
		create_warning_phrases_pool()
	var random_phrase = current_warning_pool.pop_back()
	return random_phrase

func get_random_phrase(list):
	var random_index = randi() % list.size()
	var random_phrase = list[random_index]
	return random_phrase

func start_dialogue_sequence():
	player.set_enable(true)
	npc_timer.start(10)
	yield(npc_timer, "timeout")
	services.dialogue.play_dialogue('Hey, sweet!')
	spawn_next_npc()
	npc_timer.start(10)
	yield(npc_timer, "timeout")
	services.dialogue.play_dialogue('Come here...')
	spawn_next_npc()
	npc_timer.start(10)
	yield(npc_timer, "timeout")
	services.dialogue.play_dialogue('<whistles>')
	services.audio.play_audio("whistle")
	spawn_next_npc()
	npc_timer.start(10)
	yield(npc_timer, "timeout")
	services.dialogue.play_dialogue('Talk with me, baby.')
	services.audio.play_audio("wind01", true)
	spawn_next_npc()
	npc_timer.start(10)
	yield(npc_timer, "timeout")
	services.dialogue.play_dialogue('I gonna catch youuu...')
	services.audio.play_audio("end02")
	var last_npc = spawn_next_npc()
	last_npc.set_target(player)
	open_gates()
	services.audio.play_audio("gates01")
	set_process(true)
	player.lifes = 0

func spawn_next_npc():
	if (npcs_to_spawn.size() == 0) : return
	var npc_node = npcs_to_spawn.pop_front()
	npc_node.show()
	return npc_node

func open_gates():
	animation_player.play("open_gates")

func close_gates():
	animation_player.play_backwards("open_gates")

func _on_Player_death():
	services.audio.play_audio("laught03")
	var random_dialog = get_random_phrase(final_phrases)
	services.dialogue.play_dialogue(random_dialog)
	yield(get_tree().create_timer(2), "timeout")
	var random_text = get_random_phrase(loss_text)
	emit_signal('game_end', "GAME OVER\n\n%s" % random_text)

func _on_FinalArea_body_entered(body):
	if (body.is_in_group('player')):
		var random_text = get_random_phrase(win_text)
		emit_signal('game_end', "%s" % random_text)

func _on_Player_warning():
	services.audio.play_audio("laught01")
	var random_dialog = get_warning_phrases()
	services.dialogue.play_dialogue(random_dialog)
