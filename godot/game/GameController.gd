extends Node

onready var in_game_menu = $MenuLayer/InGameMenu
onready var dialogue_box = $MenuLayer/DialogueBox
onready var end_game = $MenuLayer/EndGame
onready var start_game = $MenuLayer/StartGame
onready var player = $MainScene/Player
onready var main_scene = $MainScene
onready var cutscenes = $MainScene/Cutscenes
onready var general_audio = $GeneralAudio
onready var services = {
	"audio": general_audio,
	"dialogue": dialogue_box
}
var game_started = false

func _ready():
	main_scene.set_services(services)
	player.set_enable(false)
	randomize()

func set_control_focus(value):
	if (value):
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _unhandled_input(event):
	if (!game_started) : return
	if event is InputEventKey:
		if (Input.is_action_just_pressed("escape")):
			set_control_focus(false)
			in_game_menu.show()
			get_tree().set_pause(true)

func back_to_main_menu():
	get_tree().change_scene("res://ui/main_menu/MainMenu.tscn")

func _on_EndGame_exit():
	back_to_main_menu()

func _on_MainMenu_exit():
	back_to_main_menu()

func _on_MainMenu_resume():
	get_tree().set_pause(false)
	set_control_focus(true)

func _on_MainScene_game_end(text):
	get_tree().set_pause(true)
	end_game.set_text("%s" % text)
	end_game.show()
	set_control_focus(false)

func _on_StartGame_start():
	start_game.hide()
	cutscenes.play("start")
	main_scene.close_gates()
	general_audio.play_audio("gates01")
	set_control_focus(true)
	yield(cutscenes, "animation_finished")
	main_scene.start_dialogue_sequence()
	game_started = true
