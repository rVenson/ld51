extends KinematicBody

signal death
signal warning
const STEPS_DEFAULT_TIME = 0.7
export var mouse_sensitivity = Vector2(0.2, 0.2)
export var default_fov = 60
export var speed = 4
export var lifes = 1
onready var camera = $Camera
onready var target_direction = $TargetDirection
onready var tween = $Tween
onready var steps_sound = $StepsSound
var steps_ticket = 0
var direction = Vector3()

func _unhandled_input(event : InputEvent):
	if event is InputEventMouseMotion:
		rotate_y(deg2rad(-event.relative.x * mouse_sensitivity.y)) 
		camera.rotate_x(deg2rad(-event.relative.y * mouse_sensitivity.x)) 

func _physics_process(delta):
	direction = Vector3()
	if Input.is_action_pressed("move_forward"):
		direction -= transform.basis.z
	elif Input.is_action_pressed("move_backward"):
		direction += transform.basis.z
	if Input.is_action_pressed("move_left"):
		direction -= transform.basis.x
	elif Input.is_action_pressed("move_right"):
		direction += transform.basis.x
	direction = direction.normalized()
	move_and_collide(direction * speed * delta)
	if (direction.length() > 0):
		steps_ticket -= delta
		if (steps_ticket < 0):
			steps_sound.play(0)
			steps_ticket = STEPS_DEFAULT_TIME

func set_enable(value):
	set_physics_process(value)
	set_process_unhandled_input(value)

func set_camera_target(target_node : Spatial):
	var offset = Vector3(0, 2, 0)
	var target_position = target_node.transform.origin
	var looking_transform = transform.looking_at(target_position, Vector3.UP)
	look_at(target_position, Vector3.UP)
	camera.look_at(target_position + offset, Vector3.UP)
	camera.rotate_y(0)

func target_camera_fov(target_fov = default_fov):
	tween.interpolate_property(camera, 'fov', default_fov, target_fov, 2)
	tween.start()

func damage():
	if (lifes < 0) : return
	set_physics_process(false)
	set_camera_target(self)
	target_camera_fov(30)
	if (lifes < 1):
		emit_signal("death")
	else:
		lifes -= 1
		emit_signal("warning")
		yield(get_tree().create_timer(3), "timeout")
		target_camera_fov()
		set_physics_process(true)
