extends Control

onready var check_button = $CheckButton

func get_value():
	return bool(check_button.pressed)

func set_value(value):
	check_button.pressed = value
