extends ConfirmationDialog

onready var volume_master = $ScrollContainer/MarginContainer/Options/VolumeMaster
onready var volume_effects = $ScrollContainer/MarginContainer/Options/VolumeEffects
onready var volume_general = $ScrollContainer/MarginContainer/Options/VolumeGeneral
onready var language_config = $ScrollContainer/MarginContainer/Options/LanguageConfig

func _on_OptionsMenu_about_to_show():
	load_settings()

func _on_OptionsMenu_confirmed():
	var values = {
		"master": volume_master.get_value(),
		"effects": volume_effects.get_value(),
		"general": volume_general.get_value(),
		"strong_language": language_config.get_value(),
	}
	set_bus_volume_by_name("Master", values.master)
	set_bus_volume_by_name("Effects", values.effects)
	set_bus_volume_by_name("General", values.general)
	Global.settings.strong_language = values.strong_language

func load_settings():
	var values = {
		"master": get_bus_volume_by_name("Master"),
		"effects": get_bus_volume_by_name("Effects"),
		"general": get_bus_volume_by_name("General"),
		"strong_language": Global.settings.get('strong_language', false),
	}
	volume_master.set_value(values.master)
	volume_effects.set_value(values.effects)
	volume_general.set_value(values.general)
	language_config.set_value(values.strong_language)

func get_bus_volume_by_name(bus_name):
	var bus_id = AudioServer.get_bus_index(bus_name)
	return AudioServer.get_bus_volume_db(bus_id)

func set_bus_volume_by_name(bus_name, volume):
	var bus_id = AudioServer.get_bus_index(bus_name)
	AudioServer.set_bus_volume_db(bus_id, volume)
