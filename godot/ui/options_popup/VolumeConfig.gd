extends Control

onready var key = $Key
onready var slider_value = $SliderValue
onready var audio_stream_player = $AudioStreamPlayer

func _on_SliderValue_drag_started():
	audio_stream_player.volume_db = slider_value.value
	audio_stream_player.play(0)

func _on_SliderValue_drag_ended(value_changed):
	audio_stream_player.volume_db = slider_value.value
	audio_stream_player.play(0)

func get_value():
	return int(slider_value.value)

func set_value(value):
	slider_value.value = value
