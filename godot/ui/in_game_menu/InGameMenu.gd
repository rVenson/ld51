extends Control

signal resume
signal exit
onready var exit_confirmation = $ExitConfirmation
onready var options_menu = $OptionsMenu
onready var button_resume = $Options/ButtonResume

func show():
	visible = true
	button_resume.grab_focus()

func _on_ButtonResume_pressed():
	hide()
	emit_signal('resume')

func _on_ButtonOptions_pressed():
	options_menu.popup()

func _on_ButtonExit_pressed():
	exit_confirmation.popup()

func _on_ExitConfirmation_confirmed():
	emit_signal('exit')
