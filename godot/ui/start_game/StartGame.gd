extends Control

signal start
const DEFAULT_TYPE_TYPE = 0.05
onready var label = $Label
onready var label_2 = $Label2
onready var button_start = $Options/ButtonStart
onready var start_game_audio = $StartGameAudio
var fast_mode_activated = false
var type_time = DEFAULT_TYPE_TYPE

func _input(event : InputEvent):
	var skip = false
	skip = skip or (event is InputEventMouseButton and event.doubleclick)
	skip = skip or (event.is_action_pressed("escape"))
	if (skip):
		fast_mode_activated = !fast_mode_activated
		button_start.visible = fast_mode_activated
		if (fast_mode_activated):
			type_time = 0.005
		else:
			type_time = DEFAULT_TYPE_TYPE

func _ready():
	start_game_audio.play()
	label.percent_visible = 0
	label_2.percent_visible = 0
	yield(get_tree().create_timer(2), "timeout")
	while (label.percent_visible != 1):
		yield(get_tree().create_timer(type_time), "timeout")
		label.type()
	yield(get_tree().create_timer(2), "timeout")
	while (label_2.percent_visible != 1):
		yield(get_tree().create_timer(DEFAULT_TYPE_TYPE), "timeout")
		label_2.type()
	set_process_input(false)
	button_start.show()

func _on_ButtonStart_pressed():
	label.percent_visible = 1
	label_2.percent_visible = 1
	emit_signal("start")
