extends Control

signal exit
onready var tween = $Tween
onready var label = $Label

func set_text(text):
	label.text = str(text)

func show():
	self.modulate.a = 0
	tween.interpolate_property(self, 'modulate:a', 0, 1, 3)
	tween.start()
	visible = true

func _on_ButtonExit_pressed():
	emit_signal("exit")
