extends Label

onready var click_player = $ClickPlayer

func type():
	if (percent_visible == 1) : return
	visible_characters += 1
	click_player.play(0)
