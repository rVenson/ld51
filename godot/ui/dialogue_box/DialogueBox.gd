extends Control

const DEFAULT_DIALOGUE_TIME = 5
onready var label = $Label
var time_remaining = 0

func _process(delta):
	time_remaining -= delta
	if (time_remaining < 0):
		hide()
		set_process(false)

func play_dialogue(dialogue, actor = "STRANGER"):
	label.text = ""
	show()
	label.text = str("%s\n%s" % [actor, dialogue])
	time_remaining = DEFAULT_DIALOGUE_TIME
	set_process(true)

