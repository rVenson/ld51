extends Control

onready var value = $Value

func _process(delta):
	value.text = str(Engine.get_frames_per_second())
