extends Control

const main_scene = preload("res://game/GameController.tscn")
onready var exit_confirmation = $ExitConfirmation
onready var options_menu = $OptionsMenu
onready var version = $VBoxContainer/Version

func _ready():
	get_tree().set_pause(false)
	version.text = "%s" % ProjectSettings.get('custom/game_version')

func _on_ButtonOptions_pressed():
	options_menu.popup()

func _on_ButtonExit_pressed():
	exit_confirmation.popup()

func _on_ExitConfirmation_confirmed():
	get_tree().quit()

func _on_ButtonStart_pressed():
	get_tree().change_scene_to(main_scene)
